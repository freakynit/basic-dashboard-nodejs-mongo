var AUTO_INSERT = false;

var fs = require('fs');
var Agenda = require('agenda');
var messagingHelpers = require('./messagingHelpers');
var json2xls = require('json2xls');
var json2csv = require('json-2-csv');
var awis = require('awis');
var validator = require('validator');


var awisClient = awis({
  key: "AKIAIAIDFPAHBB36XCZQ",
  secret: "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"
});


var makeRequest = function(requestData, closeDb, cb){
	awisClient(requestData, function (err, data) {
		if(err) throw err;

        if(cb) cb(data);
	});
};


function request1_1(mDomain, mResponseGroups, cb){
	makeRequest({
	  	'Action': 'UrlInfo',
	  	'Url': mDomain,
	  	'ResponseGroup': mResponseGroups
	}, false, function(data){
		if(cb) cb(data);
	});
}



function cl(msg){
	console.log(msg);
}

var jsonToExcel = function(documents, filePath, cb){
    var xls = json2xls(documents);

    fs.writeFileSync(filePath, xls, 'binary');
    if(cb) cb();
}

var jsonToCsv = function(documents, filePath, cb){
    var options = {
        // DELIMITER : {
        //     WRAP : '"', // Double Quote (") character
        //     FIELD: ',', // Comma field delimiter
        //     ARRAY: ';', // Semicolon array value delimiter
        //     EOL  : '\n' // Newline delimiter
        // },
        PREPEND_HEADER : true,
        SORT_HEADER    : false
        // TRIM_HEADER_FIELDS: true,
        // TRIM_FIELD_VALUES:  true,
        // KEYS: ['Domain', 'Category', 'Street', 'City', 'State', 'Country', 'PostalCode']
    };

    json2csv.json2csv(documents, function(err, csv){
        if (err) throw err;
        console.log(csv);

        fs.writeFileSync(filePath, csv);
        if(cb) cb();
    }, options);
}

var sendJobDoneMail = function(recepients, fileName, filePath, cb){
    if(!cb) {
        cb = function(error, info){
            if(error) {
                cl("mail error: " + JSON.stringify(error));
            } else {
                cl("mail sent: " + JSON.stringify(info));
            }
        };
    }
    messagingHelpers.sendMailWithOptions({
            from: 'Nitin Bansal <freakynit123@gmail.com>',
            to: recepients,
            subject: "Alexa job mail",
            text: "Please find attached document. Open it in Google docs to preview",
            attachments: [
                {
                    "fileName": fileName,
                    "path": filePath
                }
            ]
        },cb);
}

var parseDomainInfo = function(mDomain, data){
    var addressBase = data.contactInfo.physicalAddress;
    var categoryPath = "";
    var physicalAddress = [];
    //var street = "", city = "", state = "", country = "", postalCode = "";

    if(data.related && data.related.categories && data.related.categories.categoryData) {
        if(Object.prototype.toString.call(data.related.categories.categoryData) == '[object Array]') {
            categoryPath = data.related.categories.categoryData[0].absolutePath;
        } else {
            categoryPath = data.related.categories.categoryData.absolutePath;
        }
    }

    if(addressBase.streets && addressBase.streets.street) {
        physicalAddress.push(addressBase.streets.street);
        //street = addressBase.streets.street;
    } else {
        physicalAddress.push("");
    }

    if(addressBase.city) {
        physicalAddress.push(addressBase.city);
        //city = addressBase.city;
    } else {
        physicalAddress.push("");
    }

    if(addressBase.state) {
        physicalAddress.push(addressBase.state);
        //state = addressBase.state;
    } else {
        physicalAddress.push("");
    }

    if(addressBase.country) {
        physicalAddress.push(addressBase.country);
        //country = addressBase.country;
    } else {
        physicalAddress.push("");
    }

    if(addressBase.postalCode) {
        physicalAddress.push(addressBase.postalCode);
        //postalCode = addressBase.postalCode;
    } else {
        physicalAddress.push("");
    }

    // street = street.replace(/,/, "_");
    // city = city.replace(/,/, "_");
    // state = state.replace(/,/, "_");
    // country = country.replace(/,/, "_");
    // postalCode = postalCode.replace(/,/, "_");

    var rowData = {
        'domain_name': mDomain,
        'category': categoryPath,
        'address': physicalAddress.join(', ')
        // 'street': street,
        // 'city': city,
        // 'state': state,
        // 'country': country,
        // 'postalCode': postalCode
    };

    return rowData;
}

var alexaHelper = {
    scheduleCustomBatchInfo: function(mDomainList, recepientsArray, responseGroups){
        var agenda = new Agenda({db: {address: "mongodb://127.0.0.1/agenda"}});
        agenda.define('domain_category_address', {priority: 'high', concurrency: 10}, function(job, done) {
            var data = job.attrs.data;
            var allDomainsData = [];

            var fetchNextDomainInfo = function(idx){
                if(idx < (data.mDomainList.length)) {
                    var mDomain = data.mDomainList[idx];
                    if(mDomain && mDomain.length > 0 && validator.isFQDN(mDomain, {'allow_underscores': true}) == true) {
                        cl("Fetching: " + mDomain);
                        request1_1(mDomain, data.responseGroups, function(data){
                            var parsedDomainInfo = parseDomainInfo(mDomain, data);
                            cl("parsedDomainInfo = " + JSON.stringify(parsedDomainInfo));
                            allDomainsData.push(parsedDomainInfo);

                            fetchNextDomainInfo(++idx);
                        });
                    } else {
                        cl("Invalid domain: " + mDomain);
                        fetchNextDomainInfo(++idx);
                    }
                } else {
                    var dateString = new Date().toUTCString().replace(/\s/ig, "_");
                    var fileName = 'allDomainInfo__' + dateString + '.csv';
                    //var fileName = 'allDomainInfo__' + dateString + '.xlsx';
                    var filePath = fileName;

                    jsonToCsv(allDomainsData, filePath, function(){
                        sendJobDoneMail(data.recepientsArray.join(","), fileName, filePath, function(error, info){
                            if(error) {
                                cl("mail error: " + JSON.stringify(error));
                                messagingHelpers.sendPushbulletNote("Alexa-Dashboard Error", "Error in sending job done mail to: " + data.recepientsArray.join(","), function(error, response){
                                    if(error) {
                                        cl("Error in sending pushbullet messag");
                                    } else {

                                    }
                                    done();
                                });
                            } else {
                                cl("mail sent: " + JSON.stringify(info));
                                done();
                            }
                        });
                    });
                }
            }

            fetchNextDomainInfo(0);
        });

        agenda.on('ready', function() {
            cl("Agenda ready");
            agenda.now('domain_category_address', {"mDomainList": mDomainList, "recepientsArray": recepientsArray, "responseGroups": responseGroups});
            agenda.start();
        });

        agenda.on('error', function() {
            cl("Agenda error");
        });
    }
};

module.exports = alexaHelper;
