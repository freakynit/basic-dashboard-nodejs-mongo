var express = require('express');
var router = express.Router();
var ObjectID = require('mongodb').ObjectID;

// Pages
router.get('/inapp', function(req, res){
  res.render('inapp/index', {title: 'Home'});
});

router.get('/inapp/notifications-list-page', function(req, res) {
  var conn = req.mongoConn;
  var collection = conn.collection('notifications');
  
  collection.find({}).toArray(function(err, notifications) {
    if(err) {
      res.json({"success": false, "error": err});
    } else {
      res.render('inapp/notifications-list-page', {title: 'Active Notifications', "notifications": notifications});
    }
  });
});

router.get('/inapp/new-inapp-page', function(req, res) {
	var conn = req.mongoConn;
	var collection = conn.collection('layouts');
	
	collection.find({}).toArray(function(err, layouts) {
  	if(err) {
  		res.json({"success": false, "error": err});
  	} else {
  		res.render('inapp/new-inapp-page', {title: 'Layouts', "layouts": layouts});
  	}
	});
});

// Form handlers
router.post('/inapp/notification/save', function(req, res) {
	var postData = req.body.postData;
	console.log("postData = ", postData);
	var conn = req.mongoConn;
	var collection = conn.collection('notifications');
	
  	collection.insert(postData, function(err, result) {
    	if(err) {
    		res.json({"success": false, "error": err});
    	} else {
    		res.json({"success": true, "inserted": result.result.n});
    	}
  	});
});

module.exports = router;
