var express = require('express');
var router = express.Router();

router.get('/app', function(req, res) {
	var users = [
		{"name": "nitin", "age": 29},
		{"name": "nitin2", "age": 30},
	];

	res.render('app/index', {title: 'The users page!', users: users});
	// res.json({"hello": "there"});
});

module.exports = router;
