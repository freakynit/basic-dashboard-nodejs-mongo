var AUTO_INSERT = false;

var express = require('express');
var router = express.Router();
var awis = require('awis');
var nodemailer = require('nodemailer');
var Agenda = require('agenda');
var PushBullet = require('pushbullet');

var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'freakynit123@gmail.com',
        pass: 'admin123A@@'
    }
});


var sendMail = function(title, message, cb){
    var mailOptions = {
        from: 'Test <freakynit123@gmail.com>',
        to: 'nitin@webklipper.com', // list of receivers
        subject: title,
        text: message
    };

    transporter.sendMail(mailOptions, cb);
};

var awisClient = awis({
  key: "AKIAIAIDFPAHBB36XCZQ",
  secret: "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"
});

function insertDocuments(datatoInsert, callback) {
  collection.insert(datatoInsert, function(err, result) {
    if(err) throw err;

    console.log("Inserted document into the document");
    callback(result);
  });
}

function filterRequestKeys(toFilter){
	var filteredKeys = {};

	for(k in toFilter){
		var newKey = k.replace(/\./g, "_");
		filteredKeys[newKey] = toFilter[k];
	}

	return filteredKeys;
}

var makeRequest = function(requestData, closeDb, cb){
	awisClient(requestData, function (err, data) {
		if(err) throw err;

		if(AUTO_INSERT === true) {
			var toInsert = {"requestData": filterRequestKeys(requestData), "response": data};
		  	insertDocuments(toInsert, function(result){
		  		if(closeDb && closeDb === true) {
		  			db.close();
		  		}

		  		if(cb) cb(data);
		  	});
		} else {
			if(cb) cb(data);
		}
	});
};

function request1_1(mDomain, mResponseGroups, cb){
	makeRequest({
	  	'Action': 'UrlInfo',
	  	'Url': mDomain,
	  	'ResponseGroup': mResponseGroups
	}, false, function(data){
		if(cb) cb(data);
	});
}

function request1_2(mDomain, mRange, mStart, cb){
	makeRequest({
	  	'Action': 'TrafficHistory',
	  	'Url': mDomain,
	  	'ResponseGroup': 'History',
	  	'Range': mRange,
	  	'Start': mStart
	}, false, function(data){
		if(cb) cb(data);
	});
}

function request1_3(mPath, mDescriptions, responseGroups, cb){
makeRequest({
	  	'Action': 'CategoryBrowse',
	  	'ResponseGroup': responseGroups,
	  	'Path': mPath,
	  	'Descriptions': mDescriptions
	}, false, function(data){
		if(cb) cb(data);
	});
}

function request1_4(mPath, mDescriptions, mSortBy, mRecursive, mCount, mStart, cb){
	makeRequest({
	  	'Action': 'CategoryListings',
	  	'ResponseGroup': 'Listings',
	  	'Path': mPath,
	  	'SortBy': mSortBy,
	  	'Recursive': true,
	  	'Count': mCount,
	  	'Start': mStart,
	  	'Descriptions': mDescriptions
	}, false, function(data){
		if(cb) cb(data);
	});

	// cb({"success": true});
}

function request1_5(mDomain, mCount, mStart, cb){
	makeRequest({
	  	'Action': 'SitesLinkingIn',
	  	'Url': mDomain,
	  	'ResponseGroup': 'SitesLinkingIn',
	  	'Count': mCount,
	  	'Start': mStart,
	}, true, function(data){
		if(cb) cb(data);
	});
}

function request1_batch(cb){
	makeRequest({
	  	Action: 'UrlInfo',
		'UrlInfo.Shared.ResponseGroup': 'Categories,ContactInfo',
		'UrlInfo.1.Url': 'lupomontero.com',
		'UrlInfo.2.Url': 'yahoo.com',
		'UrlInfo.3.Url': 'weibo.com',
		'UrlInfo.4.Url': 'github.com',
		'UrlInfo.4.Url': 'uber.com',
		'UrlInfo.4.Url': 'gitlab.com',
		'UrlInfo.4.Url': 'gmail.com',
		'UrlInfo.4.Url': 'msn.com',
		'UrlInfo.4.Url': 'hello.com',
		'UrlInfo.4.Url': 'engage.com',
		'UrlInfo.4.Url': 'mixpanel.com',
		'UrlInfo.4.Url': 'urbanairship.com',
		'UrlInfo.4.Url': 'pushwoosh.uk',
		'UrlInfo.5.Url': 'monono.org'
	}, false, function(data){
		if(cb) cb(data);
	});
}

function cl(msg){
	console.log(msg);
}

router.get('/alexa', function(req, res) {
	res.render('alexa/index');
});

router.get('/alexa/UrlInfo', function(req, res){
	var mDomain = req.query.domain_name;
	var responseGroups = req.query.response_groups;
	if(Object.prototype.toString.call(responseGroups) === '[object Array]') {
		responseGroups = responseGroups.join(",");
	}

	request1_1(mDomain, responseGroups, function(data){
		res.json(data);
	});
});

router.get('/alexa/TrafficHistory', function(req, res){
	var mDomain = req.query.domain_name;
	var mRange = req.query.range;
	var mStart = req.query.start;

	request1_2(mDomain, mRange, mStart, function(data){
		res.json(data);
	});
});

router.get('/alexa/CategoryBrowse', function(req, res){
	var mPath = req.query.path;
	var mDescriptions = req.query.descriptions;
	var responseGroups = req.query.response_groups;
	if(Object.prototype.toString.call(responseGroups) === '[object Array]') {
		responseGroups = responseGroups.join(",");
	}

	request1_3(mPath, mDescriptions, responseGroups, function(data){
		res.json(data);
	});
});

router.get('/alexa/CategoryListings', function(req, res){
	var mPath = req.query.path;
	var mDescriptions = req.query.descriptions;
	var mRecursive = req.query.recursive;
	var mSortBy = req.query.sortBy;
	var mCount = req.query.count;
	var mStart = req.query.start;

	if(Object.prototype.toString.call(mSortBy) === '[object Array]') {
		mSortBy = mSortBy.join(",");
	}

	request1_4(mPath, mDescriptions, mSortBy, mRecursive, mCount, mStart, function(data){
		res.json(data);
	});
});

router.get('/alexa/SitesLinkingIn', function(req, res){
	var mDomain = req.query.domain_name;
	var mCount = req.query.count;
	var mStart = req.query.start;

	request1_5(mDomain, mCount, mStart, function(data){
		res.json(data);
	});
});

router.get('/alexa/batch', function(req, res){
	request1_batch(function(data){
		res.json({"response": data});
	});
});

router.get('/alexa/CustomBatchInfo', function(req, res){
	var mDomain = req.query.domain_name;
	var responseGroups = 'Categories,ContactInfo';
    var longRunningString = req.query.long_running;

    if(longRunningString && longRunningString == "true") {
        var alexaHelper = require('./helpers/alexaHelper');

        var recepientsList = req.query.recepients;
        var recepientsArray = ["nitin@webklipper.com"];
        if(recepientsList && recepientsList.length > 0) {
                recepientsArray = recepientsList.split(",");
        }

        var mDomainList = req.query.domain_list;
        var mDomainArray = mDomainList.split(",");

        alexaHelper.scheduleCustomBatchInfo(mDomainArray, recepientsArray, responseGroups);
        res.json({"success": true});
    } else {
        request1_1(mDomain, responseGroups, function(data){
    		res.json(data);
    	});
    }
});

router.post('/alexa/CustomBatchInfo', function(req, res){
    try {
        var alexaHelper = require('./helpers/alexaHelper');
        var responseGroups = 'Categories,ContactInfo';
        var recepientsList = req.body.recepients;
        var recepientsArray = ["nitin@webklipper.com"];
        if(recepientsList && recepientsList.length > 0) {
                recepientsArray = recepientsList.split(",");
        }

        var mDomainList = req.body.domain_list;
        var mDomainArray = mDomainList.split(",");

        alexaHelper.scheduleCustomBatchInfo(mDomainArray, recepientsArray, responseGroups);

        res.json({"success": true});
    } catch(err){
        res.json({"success": false, "error": err.message});
    }
});


router.get('/alexa/long', function(req, res){
    var pusher = new PushBullet('rlxDyNxW9pXh3s7Zr1ofudSrYgRn01b4');
    var agenda = new Agenda({db: {address: "mongodb://127.0.0.1/agenda"}});

    var sendPushbulletNote = function(title, message, cb){
        pusher.note('ujvMdCcc8xUsjzWIEVDzOK', title, message, cb);
    }

    agenda.define('domain_category_address', {priority: 'high', concurrency: 10}, function(job, done) {
        var data = job.attrs.data;
        setTimeout(function(){
            sendMail("From Alexa-Dashboard", "hello", function(error, info){
                if(error) {
                    cl("mail error: " + JSON.stringify(error));
                } else {
                    cl("mail sent: " + JSON.stringify(info));
                }
            });

            sendPushbulletNote("this is title", "this is message", function(error, response) {
                if(error) {
                    cl("pushbullet error: " + JSON.stringify(error));
                } else {
                    cl("pushbullet response: " + JSON.stringify(response));
                }
            });

            console.log("job done");
            done();
        }, 2000);
    });

    agenda.on('ready', function() {
        cl("Agenda ready");
        agenda.now('domain_category_address');
        agenda.start();
    });

    agenda.on('error', function() {
        cl("Agenda error");
    });

    //sendMail("hello");
	res.json({"success": true, "data": "queued"});
});

router.get('/mail', function(req, res){
    cl("Request to send mail");
    sendMail("From Alexa-Dashboard", "hello", function(error, info){
        if(error) {
            cl("mail error: " + JSON.stringify(error));
        } else {
            cl("mail sent: " + JSON.stringify(info));
        }
    });

    res.json({"success": true});
});

router.get('/test', function(req, res){
    var longRunningString = req.query.long_running;
    if(longRunningString && longRunningString == "true") {
            res.json({"success": true, "data": "true"});
    } else {
        res.json({"success": true, "data": "false"});
    }
});

module.exports = router;
