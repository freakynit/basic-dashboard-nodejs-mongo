var SERVER_BASE = 'alexa/';

$.getMultiScripts = function(arr, path) {
    var _arr = $.map(arr, function(scr) {
        return $.getScript( (path||"") + scr );
    });

    _arr.push($.Deferred(function( deferred ){
        $( deferred.resolve );
    }));

    return $.when.apply($, _arr);
}

function getScripts(scripts, callback) {
    var progress = 0;
    var internalCallback = function () {
        if (++progress == scripts.length) { callback(); }
    };

    scripts.forEach(function(script) { $.getScript(script, internalCallback); });
};

function isNormalInteger(str) {
    var n = ~~Number(str);
    return String(n) === str && n > 0;
}

function getPageLoader(){
  return $("#progress-container");
}

function setProgressState(state){
  var $p = getPageLoader();

  if($p) {
    if(state == true) {
        $p.css("visibility", "visible");
    } else {
      $p.css("visibility", "hidden");
    }
  }
}

function loadTemplateWithBase(templateName, base, cb){
    var path = "templates/" + base + templateName + ".handlebars";
    loadTemplate(templateName, cb, path);
}


function loadTemplate(templateName, cb, pathToTemplate){
  setProgressState(true);

  var source;
  var template;
  var path = pathToTemplate || ("templates/" + templateName + ".handlebars");

  $.ajax({
      url: path,
      cache: true,
      success: function(data){
        setProgressState(false);
        if(cb) cb(data);
      }
  });
}

function getData(endPoint, mData, successFunction, errorFunction){
  setProgressState(true);

  $.ajax({
    url: SERVER_BASE + endPoint,
    cache: false,
    type: "get", //send it through get method
    data:mData,
    success: function(data){
      setProgressState(false);

      successFunction(data);
    },
    error: function(xhr, textStatus, errorThrown){
      setProgressState(false);
      errorFunction(xhr, textStatus, errorThrown);
    }
  });
}

function postData(endPoint, mData, successFunction, errorFunction){
  setProgressState(true);

  $.ajax({
    url: SERVER_BASE + endPoint,
    cache: false,
    type: "post",
    data:mData,
    dataType: 'json',
    contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
    success: function(data){
      setProgressState(false);
      successFunction(data);
    },
    error: function(xhr, textStatus, errorThrown){
      setProgressState(false);
      errorFunction(xhr, textStatus, errorThrown);
    }
  });
}


function getDataTemp(endPoint, mData, successFunction, errorFunction){
  setProgressState(true);
  setTimeout(function(){
    setProgressState(false);
    successFunction("hello");
  }, 1000);
}

function alexaUIHelper(templateName, templateData, templateLoadedCallback, submitButtonHandler){
  loadTemplate(templateName, function(data){
    source = data;
    template = Handlebars.compile(source);
    $dynamicSpaContainer.html(template(templateData));

    if(templateLoadedCallback) templateLoadedCallback(data);

    var $submitButton = $("#submit-button");
    var $responseContainer = $("#response-container");
    var $responseDataContainer = $("#response-data-container");
    var $responseToggleButtonsContainer = $("#response-toggle-buttons-container");

    if(submitButtonHandler != null) {
      $submitButton.click(function(){
        var requestData = submitButtonHandler();

        if(requestData !== null) {
          getData(templateName, requestData, function(data){
            $responseDataContainer.JSONView(data, {collapsed: false, nl2br: true, escape: false});
            $("#response-toggle-buttons-container #collapse-all").click(function(){
              $responseDataContainer.JSONView('collapse');
            });

            $("#response-toggle-buttons-container #expand-all").click(function(){
              $responseDataContainer.JSONView('expand');
            });

            $("#response-toggle-buttons-container #toggle").click(function(){
              var toggleValue = 2;
              var $ele = $("#response-toggle-buttons-container #toggle-value");
              if($ele != null && $ele.val().length > 0 && isNormalInteger($ele.val()) == true) {
                toggleValue = parseInt($ele.val());
              }
              $responseDataContainer.JSONView('toggle', toggleValue);
            });
          }, function(xhr, textStatus, errorThrown){
            cl("xhr, textStatus, errorThrown: " + xhr + ", " + textStatus + ", " + errorThrown);
          });
        }
      });
    }
  });
}
