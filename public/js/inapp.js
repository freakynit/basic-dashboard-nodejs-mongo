$(document).ready(function() {
	window.cmEditors= [];
	window.layoutDetails = [];

	var layoutContainers = $(".inapp-template");
	for(var i = 0; i < layoutContainers.length; i++){
		var lc = layoutContainers[i];

		var jsonData = $(lc).text();
		$(lc).empty().jJsonViewer(jsonData);
	}

	var tas = $(".inapp-data-editor-textarea");
	for(var i = 0; i < tas.length; i++){
		var $t = $(tas[i]);
		var w = $t.width();
		var h = $t.height();

		var editor = CodeMirror.fromTextArea(tas[i], {
	      lineNumbers: true,
	      mode: "javascript"
	    });

	    editor.setSize(w, h);

	    var $saveBtn = $t.parents(".block-container:eq(0)").find(".inapp-layout-data-save-button:eq(0)");
	    var btnId = $saveBtn.attr("id");
	    cmEditors[btnId] = editor;
	    //layoutDetails[btnId] = 

	    $saveBtn.click(function(e){
	    	var postData = {};
	    	var btnId = $(this).attr('id');
	    	var layoutName = btnId.substring("layout__".length);
	    	var curEditor = cmEditors[btnId];
	    	var updatedJson = JSON.parse(curEditor.getValue());

	    	postData['layout_data'] = updatedJson;
	    	for(var i = 0; i < window.layouts.length; i++){
	    		var l = window.layouts[i];
	    		if(l.layoutName == layoutName) {
	    			postData['layout_details'] = {"layoutName": layoutName};
	    		}
	    	}

	    	$.ajax({
			    type: "POST",
			    url: "/inapp/notification/save",
			    data: '{"postData": ' + JSON.stringify(postData) + '}',
			    contentType: "application/json; charset=utf-8",
			    dataType: "json",
			    success: function(data){
			    	alert("success: ", data);
			    },
			    failure: function(errMsg) {
			        alert("error: ", errMsg);
			    }
			});
	    });
	}
});
